#include <iostream>
#include <fstream>
#include <cstdlib>
#include <vector>
#include <string>
#include <conio.h>
#include <windows.h>

using namespace std;

class programs
{
		public:
		int id;
		string name;
		int length;
		int energy;
		int temp;
		int choice;
		
		programs( int pid, string pname, int plength, int penergy, int ptemp)
		{
			id=pid;
			name=pname;
			length = plength;
			energy = penergy;
			temp = ptemp;
		}
				
		void print_info()
		{
			cout<<id<<". "<<name<<" -  Length (s): "<<length<<", Energy: "<<energy<<", H2O-temp: "<<temp<<endl;
		}
		void save_data (fstream &programs_file)
		{
			programs_file<<id<<" "<<name<<" "<<length<<" "<<energy<<" "<<temp<<endl;
		}	
};


void read_programs(fstream &programs_file, vector<programs> & tab_programs)
{
	int pid;
	string name;
	int length;
	int energy;
	int temp;
	
	programs_file.open("programs.txt", ios::in);
		
	for( int i = 0; i<4;i++)
	{
		programs_file>>pid;
		programs_file>>name;
		programs_file>>length;
		programs_file>>energy;
		programs_file>>temp;
		tab_programs.push_back( programs (pid, name, length, energy, temp));

	}
		programs_file.close();
}

void read_last(fstream &programs_file, vector<programs> & tab_programs)
{
	int pid;
	string name;
	int length;
	int energy;
	int temp;
	
	programs_file.open("last.txt", ios::in);
		
	programs_file>>pid;
	programs_file>>name;
	programs_file>>length;
	programs_file>>energy;
	programs_file>>temp;
	tab_programs.push_back( programs (pid, name, length, energy, temp));

	programs_file.close();
}

void print_programs(vector<programs> & tab_programs)
	{
		for(int i = 0; i<4; i++)
		{
			tab_programs[i].print_info();
		}
	}
	
void print_last(vector<programs> & tab_programs)
	{
		tab_programs[0].print_info();
	}


void save_program(vector<programs> & tab_programs)
{
	fstream last_prog;
	int choice;
	choice--;
	last_prog.open("last.txt", ios::out);
	tab_programs[choice].save_data(last_prog);
	last_prog.close();
}


void countdown(int time)
{
	int startTime = GetTickCount();
	int pauseTime;
	int leftTime;
	char key;
	int duration;
	duration = time;
	cout<<"Press P to PAUSE or press S to STOP the washing procedure..."<<endl;
	leftTime = (startTime < (startTime + duration));
	//cout<<leftTime;
	while(GetTickCount() < (startTime + duration))
	{		
		if( kbhit() ) 
		{
		    //key = getch();
			//cout<<"Press ESC to STOP the washing procedure...";
			leftTime = ((startTime + time) - GetTickCount());
			key = getch();
			if(key == 's')
			{
				cout<<endl<<"Washing has been stopped!"<<endl;
				Sleep(2000);
				break;	
			}
			
			while(key == 'p' && leftTime > 0)
			{
				
				//pauseTime = GetTickCount();
				//cout<<"Washing was stopped at time: "pauseTime<< endl;
				cout<<endl<<"Washing is paused"<<endl<<"To resume press P again..."<<endl;
				//leftTime = ((startTime + time) - pauseTime);
				cout<<"Time left in the cycle (ms): ";
				cout<<leftTime<<endl;
				key = getch();
				if(key == 'p')
				{
					startTime = GetTickCount();
					duration = leftTime;
					cout<<"Resuming..."<<endl;
					Sleep(500);
					break;
				}
			}	
		}
			Sleep(1000);
			cout<<"#";
    }
}

int choice;
main()
{
	fstream programs_file;
	vector<programs> tab_programs;
	read_programs(programs_file, tab_programs);
		
	while(1)
	{
		system("cls");
		vector<programs> tab_last;
		fstream last_file;
		read_last(last_file, tab_last);
		cout<<"Currently chosen program: "<<endl;
		print_last(tab_last);
		cout<<"Available washing programs:"<<endl;
		print_programs(tab_programs);
	
		cout<<"Choose the program: "<<endl;

		cin>>choice;
		if(choice<1 || choice>4)
		{
			cout<<"Choose the program from 1 to 4!"<< endl;
		}
		else
		{
			save_program(tab_programs);
			cout<<"You chose the following program:"<<endl;
			choice--;
			tab_programs[choice].print_info();
			cout<<endl<<"Washing in progress!"<<endl;
			countdown(tab_programs[choice].length*1000);	
		}		
	}
}
